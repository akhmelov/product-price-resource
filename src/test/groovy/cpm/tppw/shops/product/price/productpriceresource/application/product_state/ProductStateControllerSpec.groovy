package cpm.tppw.shops.product.price.productpriceresource.application.product_state

import cpm.tppw.shops.product.price.productpriceresource.application.product_state.ProductStateController.PackProductStateRequest
import cpm.tppw.shops.product.price.productpriceresource.application.product_state.ProductStateController.WeightedProductStateRequest
import cpm.tppw.shops.product.price.productpriceresource.application.product_state.ProductStateController.UnitProductStateRequest
import cpm.tppw.shops.product.price.productpriceresource.domain.CurrencyEnum
import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.ProductRepository
import cpm.tppw.shops.product.price.productpriceresource.domain.model.shop.ShopRepository
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStockRepository
import groovy.json.JsonGenerator
import groovy.json.JsonSlurper
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.containers.GenericContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Shared
import spock.lang.Specification

import java.time.Clock
import java.time.Instant

import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.GRAM
import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.LITR
import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.NUMBER
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
class ProductStateControllerSpec extends Specification {

    private static String MONGO_PORT = '27017'
    private static String MONGO_USERNAME = 'foo'
    private static String MONGO_PASSWORD = 'secret'
    private static String MONGO_DATABASE = 'product-price-test'

    @Autowired
    protected WebApplicationContext webApplicationContext
    @Autowired
    private ProductStateInStockRepository productStateInStockRepository
    @Autowired
    private ShopRepository shopRepository
    @Autowired
    private ProductRepository productRepository

    @Autowired
    private MongoTemplate mongoTemplate;

    private MockMvc mockMvc

    private Clock now = Clock.systemDefaultZone()

    @Shared
    GenericContainer mongo = new GenericContainer("mongo:4.0")
            .withExposedPorts(Integer.valueOf(MONGO_PORT))
            .withEnv('MONGO_INITDB_ROOT_USERNAME', MONGO_USERNAME)
            .withEnv('MONGO_INITDB_ROOT_PASSWORD', MONGO_PASSWORD)
            .withEnv('MONGO_INITDB_DATABASE', MONGO_DATABASE)

    def setupSpec() {
        [
                'spring.data.mongodb.host'                   : mongo.getContainerIpAddress(),
                'spring.data.mongodb.port'                   : '' + mongo.getMappedPort(Integer.valueOf(MONGO_PORT)),
                'spring.data.mongodb.username'               : MONGO_USERNAME,
                'spring.data.mongodb.password'               : MONGO_PASSWORD,
                'spring.data.mongodb.database'               : MONGO_DATABASE,
                'spring.data.mongodb.authentication-database': 'admin'
        ].each { k, v ->
            System.setProperty(k, v)
        }
    }

    @Before
    void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def 'should insert new packed product to new shop and stock'() {
        given:
        def externalProductId = "external-product-id"
        def shopId = "shop-id"
        def stockName = "stock-name"
        def availableInStock = 23
        def stateAt = Instant.now(now)

        def prices = [packedSimplePrice(23.99f, "PLN")]
        def packedProduct = packedProduct(prices, shopId, stockName, stateAt, availableInStock)

        when:
        def createPackedProductRequest = invokePost(
                "/products/$externalProductId/states/pack",
                asRequestBody(packedProduct)
        )
        Map insertedStateResponse = contentAsMap(createPackedProductRequest)
        def createdShop = shopRepository.findById(insertedStateResponse.shopId).get()
        def createdProduct = productRepository.findById(insertedStateResponse.productId).get()
        def createdProductState = productStateInStockRepository.findById(insertedStateResponse.productStateInStockId).get()
        then:
        httpStatus(createPackedProductRequest) == 200

        createdShop.id == shopId
        createdShop.stocks.size() == 1
        createdShop.stocks[stockName].name == stockName

        createdProduct.id =~ /.+/
        createdProduct.externalId == externalProductId

        createdProductState.id =~ /.+/
        createdProductState.amountAvailable == availableInStock
        createdProductState.amountAvailableUnit == NUMBER
        createdProductState.product.externalId == externalProductId

        cleanup:
        mongoTemplate.getDb().drop()
    }

    def 'should insert new weighted product to new shop and stock'() {
        given:
        def externalProductId = "external-product-id"
        def shopId = "shop-id"
        def stockName = "stock-name"
        def weightedMass = 23.34f
        def weightedUnit = GRAM
        def stateAt = Instant.now(now)

        def prices = [weightedSimplePrice(23.99f, "PLN")]
        def weightedProduct = weightedProduct(prices, shopId, stockName, stateAt, weightedMass, weightedUnit)

        when:
        def createWightedProductRequest = invokePost(
                "/products/$externalProductId/states/weighted",
                asRequestBody(weightedProduct)
        )
        Map insertedStateResponse = contentAsMap(createWightedProductRequest)
        def createdShop = shopRepository.findById(insertedStateResponse.shopId).get()
        def createdProduct = productRepository.findById(insertedStateResponse.productId).get()
        def createdProductState = productStateInStockRepository.findById(insertedStateResponse.productStateInStockId).get()
        then:
        httpStatus(createWightedProductRequest) == 200

        createdShop.id == shopId
        createdShop.stocks.size() == 1
        createdShop.stocks[stockName].name == stockName

        createdProduct.id =~ /.+/
        createdProduct.externalId == externalProductId

        createdProductState.id =~ /.+/
        createdProductState.amountAvailable == 1
        createdProductState.amountAvailableUnit == NUMBER
        createdProductState.weightedMass == weightedMass
        createdProductState.weightedUnit == weightedUnit
        createdProductState.product.externalId == externalProductId

        cleanup:
        mongoTemplate.getDb().drop()
    }

    def 'should insert new unit product to new shop and stock'() {
        given:
        def externalProductId = "external-product-id"
        def shopId = "shop-id"
        def stockName = "stock-name"
        def amountAvailable = 23.45f
        def amountAvailableUnit = LITR
        def stateAt = Instant.now(now)

        def prices = [unitSimplePrice(23.99f, "PLN", LITR)]
        def unitProduct = unitProduct(prices, shopId, stockName, stateAt, amountAvailable, amountAvailableUnit)

        when:
        def createUnitProductRequest = invokePost(
                "/products/$externalProductId/states/unit",
                asRequestBody(unitProduct)
        )
        Map insertedStateResponse = contentAsMap(createUnitProductRequest)
        def createdShop = shopRepository.findById(insertedStateResponse.shopId).get()
        def createdProduct = productRepository.findById(insertedStateResponse.productId).get()
        def createdProductState = productStateInStockRepository.findById(insertedStateResponse.productStateInStockId).get()
        then:
        httpStatus(createUnitProductRequest) == 200

        createdShop.id == shopId
        createdShop.stocks.size() == 1
        createdShop.stocks[stockName].name == stockName

        createdProduct.id =~ /.+/
        createdProduct.externalId == externalProductId

        createdProductState.id =~ /.+/
        createdProductState.amountAvailable == amountAvailable
        createdProductState.amountAvailableUnit == amountAvailableUnit
        createdProductState.product.externalId == externalProductId

        cleanup:
        mongoTemplate.getDb().drop()
    }

    def 'should insert new packed product to new shop and stock and insert new state for the same product'() {
        given:
        def externalProductId = "external-product-id"
        def shopId = "shop-id"
        def stockName = "stock-name"
        def availableInStockBefore = 23
        def stateAtBefore = Instant.now(now)
        def availableInStockAfter = 25
        def stateAtAfter = Instant.now(now)

        def pricesBefore = [packedSimplePrice(23.99f, "PLN")]
        def packedProductBefore = packedProduct(
                pricesBefore, shopId, stockName, stateAtBefore, availableInStockBefore
        )

        def pricesAfter = [packedSimplePrice(23.99f, "PLN")]
        def packedProductAfter = packedProduct(
                pricesAfter, shopId, stockName, stateAtAfter, availableInStockAfter
        )

        when:
        def createPackedProductRequestBefore = invokePost(
                "/products/$externalProductId/states/pack",
                asRequestBody(packedProductBefore)
        )
        then:
        httpStatus(createPackedProductRequestBefore) == 200

        when:
        def createPackedProductRequestAfter = invokePost(
                "/products/$externalProductId/states/pack",
                asRequestBody(packedProductAfter)
        )
        Map insertedStateResponseAfter = contentAsMap(createPackedProductRequestAfter)
        def createdProductState = productStateInStockRepository.findById(insertedStateResponseAfter.productStateInStockId).get()
        then:
        httpStatus(createPackedProductRequestAfter) == 200

        createdProductState.id =~ /.+/
        productStateInStockRepository.count() == 2
        createdProductState.amountAvailable == availableInStockAfter
        createdProductState.amountAvailableUnit == NUMBER
        createdProductState.product.externalId == externalProductId

        cleanup:
        mongoTemplate.getDb().drop()
    }

    PackProductStateRequest.SimplePriceDto packedSimplePrice(float price, String currency) {
        new PackProductStateRequest.SimplePriceDto(price, CurrencyEnum.valueOf(currency))
    }

    WeightedProductStateRequest.SimplePriceDto weightedSimplePrice(float price, String currency) {
        new WeightedProductStateRequest.SimplePriceDto(price, CurrencyEnum.valueOf(currency))
    }

    UnitProductStateRequest.SimplePriceDto unitSimplePrice(float price, String currency, UnitEnum pricePerUnit) {
        new UnitProductStateRequest.SimplePriceDto(price, CurrencyEnum.valueOf(currency), pricePerUnit)
    }

    PackProductStateRequest packedProduct(
            List<PackProductStateRequest.PriceDto> prices,
            String shopId,
            String stockName,
            Instant stateAt,
            int availableInStock
    ) {
        new PackProductStateRequest(shopId, stockName, stateAt, prices, availableInStock)
    }

    WeightedProductStateRequest weightedProduct(
            List<WeightedProductStateRequest.PriceDto> prices,
            String shopId,
            String stockName,
            Instant stateAt,
            float weightedMass,
            UnitEnum weightedUnit
    ) {
        new WeightedProductStateRequest(shopId, stockName, stateAt, prices, weightedMass, weightedUnit)
    }

    UnitProductStateRequest unitProduct(
            List<UnitProductStateRequest.PriceDto> prices,
            String shopId,
            String stockName,
            Instant stateAt,
            float amountAvailable,
            UnitEnum amountAvailableUnit
    ) {
        new UnitProductStateRequest(shopId, stockName, stateAt, prices, amountAvailable, amountAvailableUnit)
    }

    PackProductStateRequest asRequestBody(PackProductStateRequest body) {
        return body
    }

    UnitProductStateRequest asRequestBody(UnitProductStateRequest body) {
        return body
    }

    WeightedProductStateRequest asRequestBody(WeightedProductStateRequest body) {
        return body
    }

    Map contentAsMap(MvcResult request) {
        new JsonSlurper().parseText(request.response.contentAsString)
    }

    MvcResult invokePost(String url, def body) {
        def generator = new JsonGenerator.Options()
                .addConverter(Instant) { Instant instant, String key -> instant.toString() }
                .build()

        def bodyJson = generator.toJson(body)
        mockMvc.perform(post(url).content(bodyJson).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn()
    }

    Integer httpStatus(MvcResult request) {
        request.response.status
    }
}
