package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.Product;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.Price.PriceDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.NUMBER;
import static cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStock.PRODUCT_STATE_IN_STOCK_COLLECTION;

@Document(collection = PRODUCT_STATE_IN_STOCK_COLLECTION)
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
@TypeAlias("unit-product-state")
public class UnitProductState extends ProductStateInStock {

    public UnitProductState(List<PriceDto> prices, float amountAvailable, UnitEnum amountAvailableUnit, Instant stateAt, Product product) {
        super(prices.stream().map(PriceDto::toPrice).collect(Collectors.toList()), amountAvailable, amountAvailableUnit, stateAt, product);
    }
}
