package cpm.tppw.shops.product.price.productpriceresource.domain.model.product;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Data
@Document
@RequiredArgsConstructor
public class Product {
    @Id
    private String id;

    @NotBlank
    @NonNull
    private String externalId;
}
