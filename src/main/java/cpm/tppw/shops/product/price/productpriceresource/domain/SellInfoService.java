package cpm.tppw.shops.product.price.productpriceresource.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class SellInfoService {

    public void putSellInfo(final SellInfoRequest sellInfoRequest) {

    }

    @Value
    public static class SellInfoRequest {
        private final Long shopId;
        private final Long productId;

        private final OffsetDateTime stateAt;

        private final List<Price> prices;

        @Value
        public static class Price {
            private final Float price;
            private final CurrencyEnum currency;
            private final List<Condition> conditions;

            public static abstract class Condition {
            }

            @Value
            public static class AndCondition extends Condition {
                private final Condition first;
                private final Condition second;
            }

            @Value
            public static class OrCondition extends Condition {
                private final Condition first;
                private final Condition second;
            }

            @Value
            public static class WhenBoughtOtherThanThis extends Condition {
                private final String productId;
                private final float amount;
                private final UnitEnum unit;
            }

            @Value
            public static class WhenBoughtThisGreaterThanThenBoughtNextFor extends Condition {
                private final float howManyNext;
                private final float amount;
                private final UnitEnum unit;
            }
        }
    }
}
