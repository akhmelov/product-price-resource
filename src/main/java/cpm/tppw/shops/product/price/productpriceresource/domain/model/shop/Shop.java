package cpm.tppw.shops.product.price.productpriceresource.domain.model.shop;

import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStock;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Document
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Shop {

    @Id
    @NonNull
    private String id;

    private Map<String, Stock> stocks = new HashMap<>();

    public boolean putProductStateIntoStock(ProductStateInStock productStateInStock, String stockName) {
        final boolean isStockExists = stocks.containsKey(stockName);
        stocks.computeIfAbsent(stockName, Stock::new).addNewProductState(productStateInStock);
        return isStockExists;
    }
}
