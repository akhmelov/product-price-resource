package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductStateInStockRepository extends MongoRepository<ProductStateInStock, String> {
}
