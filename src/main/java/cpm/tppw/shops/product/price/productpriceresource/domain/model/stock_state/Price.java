package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

public abstract class Price {

    public interface PriceDto {
        Price toPrice();
    }
}
