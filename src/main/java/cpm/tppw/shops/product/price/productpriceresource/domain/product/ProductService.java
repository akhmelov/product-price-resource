package cpm.tppw.shops.product.price.productpriceresource.domain.product;

import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.Product;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Product getOrCreateProduct(String externalId) {
        return productRepository
                .findByExternalId(externalId)
                .orElseGet(() -> productRepository.save(new Product(externalId)));
    }
}
