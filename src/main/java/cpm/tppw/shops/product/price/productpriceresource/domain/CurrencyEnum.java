package cpm.tppw.shops.product.price.productpriceresource.domain;

public enum CurrencyEnum {
    PLN, USD
}
