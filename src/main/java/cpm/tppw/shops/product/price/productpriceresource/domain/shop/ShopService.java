package cpm.tppw.shops.product.price.productpriceresource.domain.shop;

import cpm.tppw.shops.product.price.productpriceresource.domain.model.shop.Shop;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.shop.ShopRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ShopService {

    private final ShopRepository shopRepository;

    public Shop getShopOrCreate(String shopId) {
        return shopRepository.findById(shopId).orElseGet(() -> shopRepository.save(new Shop(shopId)));
    }
}
