package cpm.tppw.shops.product.price.productpriceresource.domain.model.shop;

import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStock;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@RequiredArgsConstructor
class Stock {

    @Id
    private String id;

    @NonNull
    private String name;
    @DBRef
    private List<ProductStateInStock> productsStateInStock = new ArrayList<>();

    public void addNewProductState(ProductStateInStock productStateInStock) {
        productsStateInStock.add(productStateInStock);
    }
}
