package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.CurrencyEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.ProductStoreType;
import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.Product;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static cpm.tppw.shops.product.price.productpriceresource.domain.ProductStoreType.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@SuperBuilder(toBuilder = true)
public abstract class ProductStateInStock {

    public static final String PRODUCT_STATE_IN_STOCK_COLLECTION = "product_in_state";

    @Id
    private String id;

    @NonNull
    private List<Price> prices;
    @NonNull
    private float amountAvailable;
    @NonNull
    private UnitEnum amountAvailableUnit;
    @NonNull
    private Instant stateAt;
    protected LocalDate expirationDate;

    @DBRef
    @NonNull
    private Product product;

//    public abstract ProductStateInStock withExpirationDate(LocalDate expirationDate); todo
}
