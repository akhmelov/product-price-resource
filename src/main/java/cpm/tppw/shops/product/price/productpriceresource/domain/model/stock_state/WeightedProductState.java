package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.Product;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.NUMBER;
import static cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStock.PRODUCT_STATE_IN_STOCK_COLLECTION;

@Document(collection = PRODUCT_STATE_IN_STOCK_COLLECTION)
@Data
@EqualsAndHashCode(callSuper = true)
@TypeAlias("weighted-product-state")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
public class WeightedProductState extends ProductStateInStock {

    private float weightedMass;
    private UnitEnum weightedUnit;

    public WeightedProductState(List<Price.PriceDto> prices, float weightedMass, UnitEnum weightedUnit, Instant stateAt, Product product) {
        super(prices.stream().map(Price.PriceDto::toPrice).collect(Collectors.toList()), 1, NUMBER, stateAt, product);
        this.weightedMass = weightedMass;
        this.weightedUnit = weightedUnit;
    }
}
