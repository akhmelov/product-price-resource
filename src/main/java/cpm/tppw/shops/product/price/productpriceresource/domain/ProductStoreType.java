package cpm.tppw.shops.product.price.productpriceresource.domain;

public enum ProductStoreType {
    PACK,
    WEIGHTED,
    UNIT
}
