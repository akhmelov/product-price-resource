package cpm.tppw.shops.product.price.productpriceresource.domain.model.shop;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ShopRepository extends MongoRepository<Shop, String> {
}
