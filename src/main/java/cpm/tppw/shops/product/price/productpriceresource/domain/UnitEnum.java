package cpm.tppw.shops.product.price.productpriceresource.domain;

public enum UnitEnum {
    KG, GRAM, LITR, NUMBER
}
