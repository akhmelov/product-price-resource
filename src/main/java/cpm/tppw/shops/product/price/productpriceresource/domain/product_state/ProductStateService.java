package cpm.tppw.shops.product.price.productpriceresource.domain.product_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.CurrencyEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.ProductStoreType;
import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.product.Product;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.shop.Shop;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.shop.ShopRepository;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.PackProductState;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.Price;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStock;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.ProductStateInStockRepository;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.SimplePrice;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.UnitProductState;
import cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state.WeightedProductState;
import cpm.tppw.shops.product.price.productpriceresource.domain.product.ProductService;
import cpm.tppw.shops.product.price.productpriceresource.domain.shop.ShopService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ProductStateService {

    private final ProductStateInStockRepository productStateInStockRepository;
    private final ProductService productService;
    private final ShopService shopService;
    private final ShopRepository shopRepository;

    public CreateProductStateResponseDto addProductState(CreateProductStateDto productStateDto) {
        final Shop shop = shopService.getShopOrCreate(productStateDto.getShopId());
        final Product product = productService.getOrCreateProduct(productStateDto.getExternalProductId());

        final ProductStateInStock productStateInStock = productStateDto.getProductStateInStock(product);
        final ProductStateInStock productStateInStockSaved = productStateInStockRepository
                .save(productStateInStock);
        shop.putProductStateIntoStock(productStateInStockSaved, productStateDto.getStockName());
        shopRepository.save(shop);
        return new CreateProductStateResponseDto(shop, product, productStateInStockSaved);
    }

    @Value
    @Builder
    public static class CreateProductStateDto {
        private final String shopId;
        private final String stockName;

        private final String externalProductId;
        private final Instant stateAt;
        private final List<PriceDto> prices;
        private final float howMuchLeft;
        private final UnitEnum unit;
        private final ProductStoreType productStoreType;

        private ProductStateInStock getProductStateInStock(Product product) {
            final List<Price.PriceDto> domainPrices = prices
                    .stream()
                    .map(PriceDto::toDtoPrice)
                    .collect(Collectors.toList());

            if (productStoreType == ProductStoreType.PACK) {
                return new PackProductState(domainPrices, (int) howMuchLeft, stateAt, product);
            } else if (productStoreType == ProductStoreType.WEIGHTED) {
                return new WeightedProductState(domainPrices, howMuchLeft, unit, stateAt, product);
            } else if (productStoreType == ProductStoreType.UNIT) {
                return new UnitProductState(domainPrices, howMuchLeft, unit, stateAt, product);
            } else {
                throw new ProductStoreTypeUnknownException();
            }
        }

        public static abstract class PriceDto {
            public abstract Price.PriceDto toDtoPrice();
        }

        @Value
        public static class SimplePriceDto extends PriceDto {
            private final float price;
            private final CurrencyEnum currency;
            private final UnitEnum pricePerUnit;

            @Override
            public Price.PriceDto toDtoPrice() {
                return new SimplePrice.SimplePriceDto(price, currency, pricePerUnit);
            }
        }
    }

    @Value
    public static class CreateProductStateResponseDto {
        private final Shop shop;
        private final Product product;
        private final ProductStateInStock productStateInStock;
    }

    public static class ProductStoreTypeUnknownException extends RuntimeException {
    }
}
