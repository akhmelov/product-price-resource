package cpm.tppw.shops.product.price.productpriceresource.domain.model.product;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product, String> {
    Optional<Product> findByExternalId(String externalId);
}
