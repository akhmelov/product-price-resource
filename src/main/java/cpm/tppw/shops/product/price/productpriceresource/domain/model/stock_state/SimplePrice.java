package cpm.tppw.shops.product.price.productpriceresource.domain.model.stock_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.CurrencyEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimplePrice extends Price {

    private float price;
    private CurrencyEnum currency;
    private UnitEnum pricePerUnit;

    @Value
    public static class SimplePriceDto implements PriceDto {
        private final float price;
        private final CurrencyEnum currency;
        private final UnitEnum pricePerUnit;

        @Override
        public Price toPrice() {
            return new SimplePrice(price, currency, pricePerUnit);
        }
    }
}
