package cpm.tppw.shops.product.price.productpriceresource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductPriceResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductPriceResourceApplication.class, args);
    }

}

