package cpm.tppw.shops.product.price.productpriceresource.application.product_state;

import cpm.tppw.shops.product.price.productpriceresource.domain.CurrencyEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum;
import cpm.tppw.shops.product.price.productpriceresource.domain.product_state.ProductStateService;
import cpm.tppw.shops.product.price.productpriceresource.domain.product_state.ProductStateService.CreateProductStateDto;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static cpm.tppw.shops.product.price.productpriceresource.domain.ProductStoreType.*;
import static cpm.tppw.shops.product.price.productpriceresource.domain.UnitEnum.NUMBER;

@RestController
@AllArgsConstructor
@RequestMapping("/products/{externalProductId}/states")
public class ProductStateController {

    private final ProductStateService productStateService;

    @PostMapping("/pack")
    public PackProductStateResponse putPackProductState(
            @PathVariable String externalProductId,
            @RequestBody @Validated PackProductStateRequest stateDto
    ) {
        return PackProductStateResponse.of(productStateService.addProductState(
                stateDto.getProductStateInStock(externalProductId)
        ));
    }

    @PostMapping("/weighted")
    public WeightedProductStateResponse putWeightedProductState(
            @PathVariable String externalProductId,
            @RequestBody @Validated WeightedProductStateRequest stateDto
    ) {
        return WeightedProductStateResponse.of(productStateService.addProductState(
                stateDto.getProductStateInStock(externalProductId)
        ));
    }

    @PostMapping("/unit")
    public UnitProductStateResponse putUnitProductState(
            @PathVariable String externalProductId,
            @RequestBody @Validated UnitProductStateRequest stateDto
    ) {
        return UnitProductStateResponse.of(productStateService.addProductState(
                stateDto.getProductStateInStock(externalProductId)
        ));
    }

    @Value
    static class PackProductStateRequest {
        private final String shopId;
        private final String stockName;

        private final Instant stateAt;
        private final List<SimplePriceDto> prices; //todo
        private final int availableInStock;

        private CreateProductStateDto getProductStateInStock(String externalProductId) {
            final List<CreateProductStateDto.PriceDto> dtoPrices
                    = prices.stream().map(PriceDto::toDtoPrice).collect(Collectors.toList());

            return CreateProductStateDto
                    .builder()
                    .shopId(shopId)
                    .stockName(stockName)
                    .externalProductId(externalProductId)
                    .stateAt(stateAt)
                    .prices(dtoPrices)
                    .howMuchLeft(availableInStock)
                    .unit(NUMBER)
                    .productStoreType(PACK)
                    .build();
        }

        public interface PriceDto {
            CreateProductStateDto.PriceDto toDtoPrice();
        }

        @Value
        public static class SimplePriceDto implements PriceDto {
            private final float price;
            private final CurrencyEnum currency;

            @Override
            public CreateProductStateDto.PriceDto toDtoPrice() {
                return new CreateProductStateDto.SimplePriceDto(price, currency, NUMBER);
            }
        }
    }

    @Value
    static class PackProductStateResponse {
        private final String shopId;
        private final String productId;
        private final String productStateInStockId;

        static PackProductStateResponse of(ProductStateService.CreateProductStateResponseDto response) {
            return new PackProductStateResponse(
                    response.getShop().getId(),
                    response.getProduct().getId(),
                    response.getProductStateInStock().getId()
            );
        }
    }

    @Value
    static class WeightedProductStateRequest {
        private final String shopId;
        private final String stockName;

        private final Instant stateAt;
        private final List<SimplePriceDto> prices; //todo
        private float weightedMass;
        private UnitEnum weightedUnit;

        private CreateProductStateDto getProductStateInStock(String externalProductId) {
            final List<CreateProductStateDto.PriceDto> dtoPrices
                    = prices.stream().map(PriceDto::toDtoPrice).collect(Collectors.toList());

            return CreateProductStateDto
                    .builder()
                    .shopId(shopId)
                    .stockName(stockName)
                    .externalProductId(externalProductId)
                    .stateAt(stateAt)
                    .prices(dtoPrices)
                    .howMuchLeft(weightedMass)
                    .unit(weightedUnit)
                    .productStoreType(WEIGHTED)
                    .build();
        }

        public interface PriceDto {
            CreateProductStateDto.PriceDto toDtoPrice();
        }

        @Value
        public static class SimplePriceDto implements PriceDto {
            private final float price;
            private final CurrencyEnum currency;

            @Override
            public CreateProductStateDto.PriceDto toDtoPrice() {
                return new CreateProductStateDto.SimplePriceDto(price, currency, NUMBER);
            }
        }
    }

    @Value
    static class WeightedProductStateResponse {
        private final String shopId;
        private final String productId;
        private final String productStateInStockId;

        static WeightedProductStateResponse of(ProductStateService.CreateProductStateResponseDto response) {
            return new WeightedProductStateResponse(
                    response.getShop().getId(),
                    response.getProduct().getId(),
                    response.getProductStateInStock().getId()
            );
        }
    }

    @Value
    static class UnitProductStateRequest {
        private final String shopId;
        private final String stockName;

        private final Instant stateAt;
        private final List<SimplePriceDto> prices; //todo
        private final float amountAvailable;
        private final UnitEnum amountAvailableUnit;

        private CreateProductStateDto getProductStateInStock(String externalProductId) {
            final List<CreateProductStateDto.PriceDto> dtoPrices
                    = prices.stream().map(PriceDto::toDtoPrice).collect(Collectors.toList());

            return CreateProductStateDto
                    .builder()
                    .shopId(shopId)
                    .stockName(stockName)
                    .externalProductId(externalProductId)
                    .stateAt(stateAt)
                    .prices(dtoPrices)
                    .howMuchLeft(amountAvailable)
                    .unit(amountAvailableUnit)
                    .productStoreType(UNIT)
                    .build();
        }

        public interface PriceDto {
            CreateProductStateDto.PriceDto toDtoPrice();
        }

        @Value
        public static class SimplePriceDto implements PriceDto {
            private final float price;
            private final CurrencyEnum currency;
            private final UnitEnum pricePerUnit;

            @Override
            public CreateProductStateDto.PriceDto toDtoPrice() {
                return new CreateProductStateDto.SimplePriceDto(price, currency, pricePerUnit);
            }
        }
    }

    @Value
    static class UnitProductStateResponse {
        private final String shopId;
        private final String productId;
        private final String productStateInStockId;

        static UnitProductStateResponse of(ProductStateService.CreateProductStateResponseDto response) {
            return new UnitProductStateResponse(
                    response.getShop().getId(),
                    response.getProduct().getId(),
                    response.getProductStateInStock().getId()
            );
        }
    }
}
